import axios from 'axios';
import {Parser} from '@json2csv/plainjs';
import {DateTime} from "luxon";
import * as fs from 'fs';
import _ from 'lodash';

//initialize account list json file if not exists

let breakLine = '\r\n';
let accountListFileName = 'accounts.json';
fs.writeFile(accountListFileName, '[]', { flag: 'wx' }, function (err) {});
var data = fs.readFileSync(accountListFileName, 'utf8');
var accountList = JSON.parse(data);
let promises = [];
for(var i=0;i<accountList.length;i++){

	let accountItem = accountList[i];
	if (accountItem.is_active){

		promises.push(
			await axios.get(accountItem.url) //https://catfact.ninja/facts
				.then((response) => {
					try {
						const parser = new Parser({header: false, delimiter:';'});
						response = response.data;
						if (_.has(response, 'balance')) {
							let balance = (isNumeric(response.balance))?parseInt(response.balance):null;
							let balanceRowItem = {
								timestamp: DateTime.now().setZone('Asia/Jakarta').toFormat('yyyy-LL-dd HH:mm:ss'),
								name: accountItem.name,
								username: accountItem.username,
								balance: balance, 
							};

							const parsedResponse = parser.parse(balanceRowItem);
							fs.appendFile('balance.csv', parsedResponse+breakLine, function (err) {
								if (err) throw err;
								console.log(parsedResponse);
							});
						} else {
						}

					} catch (err) {
						console.error(err);
						fs.appendFile('log.txt', err, function (appendErr) {
							//console.log(parsedResponse + breakLine);
						});
					}


					// handle success
					// console.log(response);
				})
				.catch(function (error) {
					// handle error
					console.log(error);
					fs.appendFile('log.txt', DateTime.now().setZone('Asia/Jakarta').toFormat('yyyy-LL-dd HH:mm:ss') +' | Cant Call: '+accountItem.url + '\n', function (appendErr) {
						//console.log(parsedResponse + breakLine);
					});
				})
				.then(function () {
					// always executed
				})
			);
		
		
		
        
    }

}

Promise.all(promises);

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}